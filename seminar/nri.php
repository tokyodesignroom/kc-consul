<div id="breadcrumb" class="clear">
	<div class="breadcrumb-link clear">
		<ul class="clear">
			<li><a href="<?php echo url_root; ?>" class="home"><span>Home</span></a></li>
			<li>&nbsp;>&nbsp;</li>
			<li><a href="http://kc-consul.com/seminar/nri.html">「野村総合研究所」 コンサルティング事業本部 キャリアセミナー開催のお知らせ</a></li>
		</ul>
	</div>
</div>


<div id="seminar_page_inc" class="clear">
	<div class="left_content l">

	<div class="content_seminar bg_while clear">
		<div class="clear"><img src="<?php echo url_root; ?>img/seminar/title-seminar.png" alt="セミナー情報"/></div>
	    <div class="content clear">
	    	<h2>
	        	<table width="750" border="0">
                <tbody><tr>
                    <td width="150">
                    <img alt="ローランド・ベルガー" src="<?php echo url_root; ?>img/seminar/logo-nri.jpg" >
                    </td>
                     <td width="30">&nbsp;
                    
                    </td>
                    <td>
                    「野村総合研究所」 コンサルティング事業本部<br>キャリアセミナー開催のお知らせ</td>
                </tr>
                </tbody>
           	</table>
    	</h2><!--End h2-->
        <div class="text">
野村総合研究所のコンサルティング事業本部にて、ビジネスパーソンを対象にした会社説明会が開催されます。<br>
説明会では、コンサルティング事業本部長から部門の事業方針やキャリア採用で求めることを説明していただくとともに、キャリア採用にてご入社され、現在コンサルタントとしてご活躍されている社員によるパネルディスカッションもございます。<br><br><br>

コンサルティング業界/NRIに転職を決めた理由、また入社後どのように働き、どのようなことを日々感じているのか等についてお話して頂ける予定です。また、守秘義務に抵触しない範囲でシンボリックなプロジェクトケースも語って頂ける予定です。コンサルティング業界、および同社の理解を深められる貴重な機会ですので、ご興味をお持ちの方は是非お気軽にご参加頂ければと思います。 
<br><br><br>

    
<table id="t-seminar">
<tbody>
<tr>
<th>日程</th>
<td>2017年5月20日（土）10:00〜12:30 (9:30受付開始)<br>
</td>
</tr>
<tr>
<th>場所</th>
<td>フィナンシャルシティグランキューブ29階大会議室<br>（参加人数により変更される可能性あり）<br>
※詳細は参加確定の方に別途ご連絡致します。
</td>
</tr>
<tr>
<th>内容</th>
<td>10:00-10:05：ご挨拶・本日の流れのご説明 <br>
10:05-10:50：コンサル業界およびNRIコンサル本部のご紹介 <br>
　　　　　　　プロジェクト事例紹介 <br>
10:50-11:50：キャリア社員によるパネルディスカッション <br>
　　　　　　　質疑応答<br>
　　　　　　　（中途入社の若手・ 中堅社員の方2〜3名が参加予定） <br>
11:50-12:00：選考について・アンケート記入 <br>
12:00- 　 　&nbsp;：閉会・質問会（自由参加）<br><br>

※プログラムは変更される可能性があります。
</td>
</tr>
<tr>
<th>費用</th>
<td>
無料
</td>
</tr>
<tr>
<th>申込方法</th>
<td>
既に当社（クライス＆カンパニー）へご登録済みの方は担当コンサルタントへご連絡下さい。
ご登録がまだの方は「野村総合研究所セミナー希望」と明記の上、<a href="mailto:kc-consul@kandc.com" style="opacity: 1;">kc-consul@kandc.com</a>までメールでお申込みください。
</td>
</tr>
<tr>
<th>申込締切</th>
<td>
<STRONG style="color:red;">2017年5月12日（金）</STRONG>　
</td>
</tr>
<tr>
<th>対象</th>
<td>
・四年制大学／大学院をご卒業後、企業または官公庁での実務経験が3年以上の方<br>
※申し込み多数の場合は抽選とさせて頂きます。予めご了承ください。
</td>
</tr>
<tr>
<th>備考</th>
<td>
・ご不明な点やご質問等ございましたらご相談ください。<br>
・当日は時間厳守でお願いいたします。
</td>
</tr>
</tbody> 
</table>
	
	   </div><!--End text-->
	    </div><!--End content width=750px;-->
	</div><!--End content_seminar-->


</div><!--left_content-->