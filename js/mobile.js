jQuery(document).ready(function($) {
	
	
        $('#main-slider').nivoSlider({
			controlNav: false,
			directionNav: false,
		})
    
	
	//Show / hide primary mobile menu
		$(".mobile_menu_btn").click(function(){
			if($(this).hasClass("open_icon")){
				$(this).removeClass("open_icon").addClass("close_icon");
				$("#mobile_primary_menu").stop().slideDown(500);
			}
			else{
				$(this).removeClass("close_icon").addClass("open_icon");
				$("#mobile_primary_menu").stop().slideUp(500);
			}
		});
		
		//Show / hide footer mobile menu
		$(".open_footer_menu_btn").click(function(){
			$("#mobile_footer_menu").stop().slideToggle();
		});
		
		//Back to top
		$(".back_top_btn").click(function(){
			$('body,html').animate({scrollTop:0});
		});
		
		//slideToggle
	$(".toggle_btn").click(function(){
		$(this).closest('.job_list_parent_item li').find('.category_jobs_parent_toggle').toggleClass('block_active');
		$(this).closest('.job_list_parent_item li').find('.job_list_child').slideToggle();
    });
	
	//toggle_menu
	$(".toggle_menu").click(function(){
		$(this).toggleClass('toggle_menu_active');
    });
	
	//backnumber toggle
	$(".backnumber_toggle_btn").click(function(){
		$(this).closest('.group_slide_bar_public').find('.backnumber_toggle_btn').toggleClass('backnumber_active');
		$(this).closest('.group_slide_bar_public').find('.backnumber_toggle_content').slideToggle();
    });
	
	//interview toggle
	$(".interview_toggle_btn").click(function(){
		$(this).closest('.group_interview_parent').find('#filterOptions').slideToggle();
    });
		
		
		
    $(window).resize(function(){

       if ($(window).width() <= 767) {  
            //$(".button_entry").append('');
       }     

});
});