<?php 
	$currentpage = '';
	 	if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
			 $currentpage = (int)$_GET['currentpage'];
		}else{
			 $currentpage = 1;
		} 
	?>
<title>ファンド（PE・VC)の求人情報 <?php echo $currentpage; ?>ページ |コンサルタント転職、ポストコンサルタント転職 クライス＆カンパニー</title>
<meta name="keywords" content="コンサルタント,コンサルティング,戦略コンサル,ITコンサル,ポストコンサル,EXコンサル,転職,人材紹介,エージェント,求人,ファンド（PE・VC)" />
<meta name="description" content="ファンド（PE・VC)の求人情報です。コンサルティングファームの求人案件はもちろん、年収800万円以上の高年収、管理職、スペシャリストの事業会社案件も豊富です。" />
<meta property="og:title" content="ファンド（PE・VC)の求人情報 <?php echo $currentpage; ?>ページ |コンサルタント転職、ポストコンサルタント転職 クライス＆カンパニー" />
<meta property="og:description" content="ファンド（PE・VC)の求人情報です。コンサルティングファームの求人案件はもちろん、年収800万円以上の高年収、管理職、スペシャリストの事業会社案件も豊富です。" />
<meta property="og:image" content="<?php echo url_root; ?>img/index/facebook-og-image.png" />
                 

<?php $url_curent=curPageURL();
/*
		if (strpos($url_curent,'//www.') !== false) {
    		$url_kandc_canon=str_replace("kc-consul.com","kandc.com",$url_curent);
		}else{
			$url_kandc_canon=str_replace("kc-consul.com","www.kandc.com",$url_curent);
		}
	*/	
	?>
<link rel="canonical" href="<?php echo $url_curent; ?>" />

