<?php 
$Data_interview_detail=Get_Data("kc_interview"," GROUP BY  `kc_interview_vol_number` ORDER BY  `kc_interview_id` DESC LIMIT 1");
$id_interview=$Data_interview_detail['kc_interview_id'];

$Data_category=Get_Data("kc_category_interview as A join category_join_kc_interview as B on A.kc_category_interview_id=B.kc_category_interview_id"," and A.kc_category_interview_status=0 and B.kc_interview_id=$id_interview");

    $category_name_ja=$Data_category['kc_category_interview_name_ja'];
	$category_name_en=$Data_category['kc_category_interview_name_en'];
	$title_interview=$Data_interview_detail['kc_interview_title'];
	$inter_vol=$Data_interview_detail['kc_interview_vol_number_slug'];
	
	$content_postion=unserialize($Data_interview_detail['kc_interview_postion']);
	$content_array_postion=@array_filter($content_postion);
	if(!empty($content_array_postion)):
		$group_postion="";
		$group_postion_inter_full="";
	for($t=0;$t<count($content_array_postion);$t++):
		$postion_ic=$content_array_postion[$t]['postion'];
		$interviewee_fullname_ic=$content_array_postion[$t]['interviewee_fullname'];
		$group_postion .=	$postion_ic." ".$interviewee_fullname_ic." ";
		$group_postion_inter_full .=$interviewee_fullname_ic." ";
		$group_postion_breakcrurm .=$interviewee_fullname_ic." 氏&";
		$group_postion_detail_first .=$postion_ic." ".$interviewee_fullname_ic."氏<br/>";
		
		$group_postion_detail_first_change .="<p class='list_group_interfull clear'><span class='postion_span'>".$postion_ic."</span><span class='interviewfull_span'>".$interviewee_fullname_ic."氏</span></p>";


	endfor;
	$postion_string_end= substr($group_postion,0,-1);
	$group_postion_detail_first_show=substr($group_postion_detail_first,0,-5);

	$group_postion_breakcrurm_show=substr($group_postion_breakcrurm,0,-1);
	endif;
	
	$page_number_inter=$Data_interview_detail['kc_interview_page_number'];
	$company_name=$Data_interview_detail['kc_interview_company_name'];
	
	$Data_list_interview_check=To_Get_Data("kc_interview","  and kc_interview_vol_number_slug='$inter_vol' order by kc_interview_page_number ASC");
	$count_page_navi=$Data_list_interview_check['cnt'];
	
?>


<div id="breadcrumb" class="clear">
     <div class="breadcrumb-link clear">
         <ul class="clear">
            <li><a href="<?php echo url_root; ?>" class="home"><span>Home</span></a></li>
            <li>&nbsp;>&nbsp;</li>
            <li><a href="<?php echo url_root; ?>interview/">インタビュー</a></li>
            <li>&nbsp;>&nbsp;</li>
            <li><a href="<?php echo url_root; ?>interview/#<?php echo $category_name_en; ?>"><?php echo $category_name_ja; ?></a></li>
            <li>&nbsp;>&nbsp;</li>
            <li><?php echo $Data_interview_detail['kc_interview_company_name']." ".$group_postion_breakcrurm_show; ?></li>
          </ul>
     </div>
</div>

<?php 
	if(!empty($Data_interview_detail['kc_interview_main_img'])):
?>
<div class="clear img_content_main <?php if($Data_interview_detail['kc_interview_page_number']==1): echo "firts_page_main_img"; else: echo "other_page_main_img"; endif ?>">
	<img class="pc" alt="<?php echo $Data_interview_detail['kc_interview_main_alt']; ?>" src="<?php echo url_root_main.$url_images_thumnail.$Data_interview_detail['kc_interview_main_img']; ?>"/>
	<?php if(!empty($Data_interview_detail['kc_interview_main_img_mobile'])):
			$img_main_top_detail=$Data_interview_detail['kc_interview_main_img_mobile'];
			$img_main_top_detail_alt=$Data_interview_detail['kc_interview_main_alt_mobile'];
	  else:
			$img_main_top_detail=$Data_interview_detail['kc_interview_main_img'];
			$img_main_top_detail_alt=$Data_interview_detail['kc_interview_main_alt'];
	endif;
	 ?>
	<img class="mobile" alt="<?php echo $img_main_top_detail_alt; ?>" src="<?php echo url_root_main.$url_images_thumnail.$img_main_top_detail; ?>"/>

</div>
<?php 
	endif;
?>
	<div class="interview_content clear <?php if($Data_interview_detail['kc_interview_page_number']==1): echo "first_page_interview_content"; else: echo "other_page_interview_content"; endif ?>">
		<div class="left_content l">
	    	<div id="interview_content_left" class="clear bg_while">
				<div class="clear group_entry_inter_head">
				<?php 
					$link_releated=str_replace(" ","",$Data_interview_detail['kc_interview_realeated_link']);
					//echo $Data_interview_detail['kc_interview_realeated_link'];
					if(!empty($link_releated)):
				?>
			  <div class=" clear">
                <div class="another-post">
				<a href="<?php echo $Data_interview_detail['kc_interview_realeated_link']; ?>">
					<?php 
						$hiden=(int)$Data_interview_detail['kc_interview_realeated_link_check_hide'];
						if($hiden==1):
					?>
	                	＞&nbsp;前編はこちら
	                <?php else: ?>
	                	＞&nbsp;後編はこちら
	                <?php endif; ?>	
				</a>
				</div></div>
            	<?php 
					endif;
				?>
            	<!--<p class="interview_date">
                </?php 
				$semina_date_d=(int)date('d', strtotime($Data_interview_detail['kc_interview_date']));
				$semina_date_m=(int)date('m', strtotime($Data_interview_detail['kc_interview_date']));
				$semina_date_y=(int)date('Y', strtotime($Data_interview_detail['kc_interview_date']));
				$semina_date=$semina_date_y."年&nbsp;".$semina_date_m."月&nbsp;".$semina_date_d."日"; 
				?>
                掲載日:</?php echo $semina_date; ?>
                </p>date interview-->
				
                <div class="mobile interview_detail_top_txt">
                <p class="interview_detail_vol"><?php echo $Data_interview_detail['kc_interview_vol_number']; ?></p>
				<h1 class="interview_detail_title_mobile"><?php echo $Data_interview_detail['kc_interview_title']; ?></h1>
                </div>
                <h3 class="interview_detail_title"><?php echo $Data_interview_detail['kc_interview_company_name']; ?></h3>
                <h4 class="interview_detail_subtitle">
                	<?php 
								echo $group_postion_detail_first_change;
								?>
                </h4>
			    </div><!--group_entry_inter_head-->
                <?php 
					if(!empty($Data_interview_detail['kc_interview_excerpt']) && $Data_interview_detail['kc_interview_excerpt']!=""):
				?>
                <div class="content-lead clear">
                	<?php echo htmlspecialchars_decode($Data_interview_detail['kc_interview_excerpt']); ?>
                </div>
				<?php 
					endif;
				?>
                
               <div id="interview-contents" class="clear"> 
                <?php 
					$content_json=unserialize($Data_interview_detail['kc_interview_content']);
					$content_array_group=@array_filter($content_json);
					if(!empty($content_array_group)):
						for($j=0;$j<count($content_array_group);$j++):
							echo "<div class='container_list_item_content clear'>";
							$group_item_array=@array_filter($content_array_group[$j]['group_item']);
							//print_r($group_item_array);
							$count_group_item=count($group_item_array);
							?>
                            	<div class="clear green b content_title_view"><?php echo $content_array_group[$j]['subtitle']; ?></div>
								<div class="clear container_content_kc">
	                            <?php 
										for($u=0;$u<$count_group_item;$u++):
								?>
										<dl class="clear">
	                                	<dt><?php echo $group_item_array[$u]['content_left']; ?></dt>
										<dd><?php
										$string_right_content=$group_item_array[$u]['content_right'];
										$string_right_content=str_replace("\n","<br/>",$string_right_content);
										$string_right_content=preg_replace("/(\s)/", " ", $string_right_content);
										 $string_right_content_img_alt=$group_item_array[$u]['content_alt_img'];
										 $string_right_conten_img_src=$group_item_array[$u]['content_src_img'];
										 $string_right_content_img_postion=$group_item_array[$u]['content_postion_img'];
										 if(!empty($string_right_conten_img_src)):
										 ?>
                                         <img src="<?php echo url_root_main.$url_images_thumnail.$string_right_conten_img_src; ?>"  <?php if(!empty($string_right_content_img_alt)): ?> alt="<?php echo $string_right_content_img_alt; ?>" <?php endif; ?> class="img_data <?php if($string_right_content_img_postion=='left'): echo "left_auto_img"; else: echo "right_auto_img"; endif; ?>"/>
                                         <?php
										 endif;
										  echo $string_right_content;
										  ?></dd></dl>
	                                <?php 
										endfor;
									?>
	                            </div><!--item content-->
                            <?php
							echo "</div>";
						endfor;
					endif;
				?>
              </div>  
              
                <?php 
					//$Data_list_interview_check=To_Get_Data("kc_interview"," and kc_interview_vol_number_slug='$vol' order by kc_interview_vol_number ASC");
					//$count_page_navi=$Data_list_interview_check['cnt'];
					if($count_page_navi>0):
				?>
                <div class="loading-info"><div class="loading-pulse"></div></div>
				<input type="hidden" value="1" id="curent_page_load" autocomplete="off"/>
				<input type="hidden" value="<?php echo url_root; ?>interview/<?php echo $category_name_en; ?>/<?php echo $inter_vol; ?>/" id="curent_link_load" autocomplete="off"/>
				<input type="hidden" value="<?php echo $count_page_navi; ?>" id="max_count_page" autocomplete="off"/>
                <?php 
				 	endif;
				 ?>
                 
                 
                 <?php
			if(!empty($Data_interview_detail['kc_interview_realeated_link'])):
				 $hiden=(int)$Data_interview_detail['kc_interview_realeated_link_check_hide'];
				  if($count_page_navi==$page_number && $hiden!=1 ):?>
                 <div class="another-post-bottom clear">
                <div class="another-post">
				<a href="<?php echo $Data_interview_detail['kc_interview_realeated_link']; ?>">
					<?php 
						
						if($hiden==1):
					?>
	                	＞&nbsp;前編はこちら
	                <?php else: ?>
	                	＞&nbsp;後編はこちら
	                <?php endif; ?>	
				</a>
				</div>
                </div>
                <?php endif; endif; ?>
                 
                 <p class="interview_date">※インタビュー内容、企業情報等はすべて取材当時のものです。</p>
                 
                <?php 
						$url_social="";
					if($page_number==1):
						$url_social=$pageURL_cu;
					else:
						$url_social=url_root."interview/";
					endif;
					
				?>	
                    <div class="social_interview clear">
                        	<ul class="clear social_list_item r">
								<li style="width: 90px;">
			<div class="fb-share-button" data-href="<?php echo $url_social; ?>" data-layout="button_count"></div>		
								</li>
			<?php /*?>
                        		<li>
            <div class="fb-like" data-href="<?php echo $url_social; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                                </li>
                        		<li>
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $url_social; ?>" data-lang="ja">ツイート</a>
                                </li>
                        		<li>
             <div class="g-plusone" data-size="medium" data-href="<?php echo $url_social; ?>"></div>
                                </li>
			<?php */?>
                        	</ul>
                        </div><!--social_break_top_left-->
                <?php
                	if((int)$Data_interview_detail['kc_interview_company_id']>0):
				?>
                	<div class="interview_entry_btn clear">
	                    <a href="http://kc-consul.com/entry/?entry_id=<?php echo $Data_interview_detail['kc_interview_company_id']; ?>" target="_blank">
	                    	<img class="pc" src="<?php echo url_root; ?>img/entry/company-id-button.png" alt="この企業を指名して転職支援を申し込む" />
                            <img class="mobile" src="<?php echo url_root; ?>img/entry/company-id-mobile-button.png" alt="この企業を指名して転職支援を申し込む" />
	                    </a>
                    </div><!--interview_entry_btn-->
                <?php 
					endif;
				?>
                
            </div><!--#interview_content_left-->


			<?php 
				$link_roundtable_array=unserialize($Data_interview_detail['link_roundtable']);
				$link_roundtables=@array_filter($link_roundtable_array);
				if(!empty($link_roundtables)):
					echo '<ul class="clear related_list_interview">';
						$count_link_round=count($link_roundtables);
						for($mk=0;$mk<$count_link_round;$mk++):
							?>
                            <li>
	                            <a href="<?php echo $link_roundtables[$mk]['link']; ?>" target="_top">
	                            	<span class="fist_link"><?php echo $link_roundtables[$mk]['main']; ?></span>
	                            	<span class="two_link"><?php echo $link_roundtables[$mk]['title']; ?></span>
	                            </a>
	                            <a target="_top" href="<?php echo $link_roundtables[$mk]['link']; ?>" class="read_more_career_consultant_sidebar r">
	                            	<span>記事を見る</span>
	                            	<span class="next_btn"></span>
	                            </a>
                            </li>
                            <?php
						endfor;
					echo "</ul>";
				endif;
			?>

	    </div><!--left_content-->
        
        <div class="right_content r">
        	<?php @include('inc/slide_bar_interview_backnumber.php'); ?>
        </div><!--right_content-->
        
	</div><!--interview_content-->

<style type="text/css">
	.interview_hide_add{
	 display:none !important;
}
.main_img_inthub {
	margin-left:-30px;
	margin-right:-30px;
	padding:20px 0 15px 0;
}
@media screen and (max-width: 767px){
	.main_img_inthub {
		margin-left:-3%;
		margin-right:-3%;
		padding:3% 0 2% 0;
	}
}
</style>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$.ajaxSetup({ cache: false });
		var loading  = true; 
		<?php //if($number_page==1): ?>
		load_contents();
		<?php //endif; ?>
		//Ajax load function
		function load_contents(){
			
			var max_page=$('#max_count_page').val();
			var url_current=$('#curent_link_load').val();
			var track_page=parseInt($('#curent_page_load').val())+1; //page number increment
			
			
			var url_set=url_current+track_page+'.html';
			
				if(track_page<=max_page && loading == true){
					   //alert('max_page:'+max_page+'url_current:'+url_current+'track_page:'+track_page+'url_set:'+url_set);
		  		       loading = false;  //set loading flag on
		  				$.ajax({
		  			        url: url_set,
		  			        type: "POST", 
		  			        dataType:"html",
							data: { valis: "also"},
		  					cache: false,
		  					headers: {
		  				       'Cache-Control': 'no-cache, no-store, must-revalidate', 
		  				       'Pragma': 'no-cache', 
		  				       'Expires': '0'
		  				     },
		  			        beforeSend: function() {
		  					    $('.loading-info').show(); //show loading animation 
		  					},
		  		          	success:function(data) {
								
		  						$('.loading-info').hide();
		  						//var set_page=track_page+1;
		  						$('#curent_page_load').val(track_page);
		  						var content_main_img= $(data).find(".img_content_main").html();
		  			            var content_value= $(data).find("#interview-contents").html();
		  						
		  						//console.log($(data).find("#main_image_top_page"));
		  						 $('#interview-contents').append('<div class="main_img_inthub clear">'+content_main_img+'</div>');
		  			            $('#interview-contents').append(content_value);
		  						loading = true; //set loading flag off once the content is loaded
								load_contents();
		  			        },
		  			        error: function(errorThrown){
		  			            alert(errorThrown); 
		  			        }
		  			    });  
		  				
		  			}//if/else
				
		}//endfunction
	});
</script>
