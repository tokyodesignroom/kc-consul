<div id="top_index_slider">
    	<ul class="clear">
        
         <li><a href="<?php echo url_root; ?>interview/symposium/vol32/1.html"><img alt="NRIだからこそ発揮できる総合力で、お客様に密着し、期待に応える。NRI 野村総合研究所 システムコンサルティング事業本部 李 智慧氏 木部 雄一氏 村川 友章氏" src="<?php echo url_root; ?>img/slide/vol32-nri.png" width="980" height="345" /></a></li>
        
      
         <li><a href="<?php echo url_root; ?>interview/symposium/vol30/1.html"><img alt="日本の未来のために、 真にイノベーションを起こしたい。 だから我々はこの場を選んだ。 トーマツ ベンチャーサポート株式会社 瀬川 友史氏&森山 大器氏" src="<?php echo url_root; ?>img/slide/vol30-deloitte-slide.png" width="980" height="345" /></a></li>
        
   
        
        <li><a href="<?php echo url_root; ?>interview/top/vol27/1.html"><img alt="日本発のプロフェッショナルサービスで、コンサルティングの歴史さえ変えていきたい。 株式会社フィールドマネージメント 並木 裕太氏" src="<?php echo url_root; ?>img/slide/vol27-field-management.png" width="980" height="345" /></a></li>
        

          
	        <li><a href="<?php echo url_root; ?>interview/consultant-post/vol02/1.html"><img alt="コンサルティング会社で経営を学び経営者、投資ファンド代表へキャリアアップ　シティック・キャピタル・パートナーズ・ジャパン・リミテッド日本代表　中野 宏信氏 " src="<?php echo url_root; ?>img/slide/vol02-citic-capital-slide.png" width="980" height="345" /></a></li>
    
      
</ul>
      
    <div class="fix_np">
	<p id="prev" style="visibility: visible;"><img width="30" height="59" class="img_p" alt="" src="<?php echo url_root; ?>img/slide/arrow-left.png"></p>
	<p id="next" style="visibility: visible;"><img width="30" height="59" class="img_n" alt="" src="<?php echo url_root; ?>img/slide/arrow-right.png"></p>
    </div>
</div><!-- /slider -->

			<div id="main-slider" class="clear mobile_slishow">
            	<a href="<?php echo url_root; ?>interview/symposium/vol32/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v32.jpg" alt="" title="#htmlcaption1" /></a>
	           	 <a href="<?php echo url_root; ?>interview/symposium/vol30/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v30.jpg" alt="" title="#htmlcaption2" /></a>
                 <a href="<?php echo url_root; ?>interview/top/vol27/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v27.jpg" alt="" title="#htmlcaption3" /></a>
                 <a href="<?php echo url_root; ?>interview/consultant-post/vol02/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v2.jpg" alt="" title="#htmlcaption4" /></a>
                
            </div> <!--main-slider-->
            
            <div id="htmlcaption1" class="nivo-html-caption">
               <p class="gold_text">NRIだからこそ発揮できる総合力で、お客様に密着し、期待に応える。</p>
               <p class="white_text">NRI 野村総合研究所 システムコンサルティング事業本部 李 智慧氏 木部 雄一氏 村川 友章氏</p>
            </div>
            
            <div id="htmlcaption2" class="nivo-html-caption">
               <p class="gold_text">日本の未来のために、 真にイノベーションを起こしたい。 だから我々はこの場を選んだ。 </p>
               <p class="white_text">トーマツ ベンチャーサポート株式会社 瀬川 友史氏&森山 大器氏"</p>
            </div>
            
             <div id="htmlcaption3" class="nivo-html-caption">
               <p class="gold_text">日本発のプロフェッショナルサービスで、コンサルティングの歴史さえ変えていきたい。</p>
               <p class="white_text">株式会社フィールドマネージメント 並木 裕太氏"</p>
            </div>
            
             <div id="htmlcaption4" class="nivo-html-caption">
               <p class="gold_text">コンサルティング会社で経営を学び経営者、投資ファンド代表へキャリアアップ</p>
               <p class="white_text">シティック・キャピタル・パートナーズ・ジャパン・リミテッド</p>
            </div>
             

<script src="<?php echo url_root; ?>js/jquery.easing.min.js" type="text/javascript"></script>
<script src="<?php echo url_root; ?>js/utility.js" type="text/javascript"></script>
<script src="<?php echo url_root; ?>js/jquery_slider_home.js" type="text/javascript"></script>