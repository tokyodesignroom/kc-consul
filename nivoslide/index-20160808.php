<div class="top_slider clear">
			<div id="main-slider" class="clear">
            	<a href="<?php echo url_root; ?>interview/symposium/vol32/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v32.jpg" alt="" title="#htmlcaption1" /></a>
	           	 <a href="<?php echo url_root; ?>interview/symposium/vol30/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v30.jpg" alt="" title="#htmlcaption2" /></a>
                 <a href="<?php echo url_root; ?>interview/top/vol27/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v27.jpg" alt="" title="#htmlcaption3" /></a>
                 <a href="<?php echo url_root; ?>interview/consultant-post/vol02/1.html"><img src="<?php echo url_root; ?>img/slide/slide-m-v2.jpg" alt="" title="#htmlcaption4" /></a>
            </div> <!--main-slider-->
            <p class="index_catch"><img src="<?php echo url_root; ?>img/index/index-catch-kreis-company-pc.png" alt="" title="" /></p>
</div>
            
            <div id="htmlcaption1" class="nivo-html-caption">
               <p class="gold_text">NRIだからこそ発揮できる総合力で、お客様に密着し、期待に応える。</p>
               <p class="white_text"><span class="position">NRI 野村総合研究所 システムコンサルティング事業本部 李</span><span class="name">智慧氏 木部 雄一氏 村川 友章氏</span></p>
            </div>
            
            <div id="htmlcaption2" class="nivo-html-caption">
               <p class="gold_text">日本の未来のために、 真にイノベーションを起こしたい。 だから我々はこの場を選んだ。 </p>
               <p class="white_text"><span class="position">NRI 野村総合研究所 システムコンサルティング事業本部 李</span><span class="name">智慧氏 木部 雄一氏 村川 友章氏</span></p>
            </div>
            
             <div id="htmlcaption3" class="nivo-html-caption">
               <p class="gold_text">日本発のプロフェッショナルサービスで、コンサルティングの歴史さえ変えていきたい。</p>
               <p class="white_text"><span class="position">NRI 野村総合研究所 システムコンサルティング事業本部 李</span><span class="name">智慧氏 木部 雄一氏 村川 友章氏</span></p>
            </div>
            
             <div id="htmlcaption4" class="nivo-html-caption">
               <p class="gold_text">コンサルティング会社で経営を学び経営者、投資ファンド代表へキャリアアップ</p>
               <p class="white_text"><span class="position">NRI 野村総合研究所 システムコンサルティング事業本部 李</span><span class="name">智慧氏 木部 雄一氏 村川 友章氏</span></p>
            </div>
             