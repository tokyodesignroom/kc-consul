<!--Job info-->
<script type="text/javascript">
		
        $(document).ready(function(){
			$(window).on('load resize', function () {
				var vs_767= window.matchMedia("only screen and (max-width: 767px)");
				if(vs_767.matches)
				{
					$("ul.job_list_child_fix li:nth-child(1) .cat_child_1").attr('style','width:inherit !important');
					$("ul.job_list_child_fix li:nth-child(2) .cat_child_1").attr('style','width:inherit !important');
					$("ul.job_list_child_fix li:nth-child(3)").attr('style','width:100% !important');
					
					$("ul.job_list_child_fix li:nth-child(3) .cat_child_1").attr('style','width:inherit !important');
		
					$("ul.job_list_child_fix li:nth-child(4)").attr('style','width:100% !important;margin-left:0px;');
					$("ul.job_list_child_fix li:nth-child(4) .cat_child_1").attr('style','width:inherit !important;');

				}else{
					$("ul.job_list_child_fix li:nth-child(1) .cat_child_1").attr('style','width: 100% !important');
					$("ul.job_list_child_fix li:nth-child(2) .cat_child_1").attr('style','width:100% !important');
					$("ul.job_list_child_fix li:nth-child(3)").attr('style','width:47% !important');
					
					$("ul.job_list_child_fix li:nth-child(3) .cat_child_1").attr('style','width:100% !important');
		
					$("ul.job_list_child_fix li:nth-child(4)").attr('style','width:40% !important');
					$("ul.job_list_child_fix li:nth-child(4) .cat_child_1").attr('style','width:100% !important;');
				}
			});
		});
    
    
    </script>



        	<div class="job_info_list clear job_info_list_index" style="margin-top:0px;">
            <div class="clear title_group_job_top">
            	<div class="l title_job_top">
            		<img src="<?php echo url_root; ?>img/job-main-title.png" alt="求人情報"/>
            	</div>
                <!--<div class="l title_job_end">
                	公開可能な求人から厳選して掲載しています。
                </div>-->
            </div>



			 <?php if($only_index=='category_public'): ?>
            	<div class="clear picup_job_index">
                   <div class="clear group_picup_job_index_b">
                	<h4 style="color:#b39f68; font-size:17px;">厳選注目求人<!--<img src="<?php echo url_root; ?>img/home/title-pickup-job.png" alt="Pick up job"/>--></h4>
                   
       
                   <ul class="clear list_item_picup" >
                   
                   <li><span class="title">外資系ITベンダー　ビジネス・コンサルタント（事業戦略領域）　～2000万円程度（非公開）</span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1030419">大手決済系企業　グローバル・インベストメント事業開発　～1200万円程度</a></span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1009122">マーケティング系ファーム　ディレクタークラス　～2300万円</a></span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1030448">外資系消費財メーカー　ストラテジープランニングストラテジープランニング　～1300万円</a></span></li>
                   <li><span class="title">外資系製薬企業　IT部門マネージャー　～1300万円（非公開）</span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1030486">日系ITベンダー　経営企画(代表取締役CEO直轄部門)　～1500万円程度</a></span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1025517">大手小売企業　事業コンサルティング・改革リーダー　～2000万円程度</a></span></li>
                   <li><span class="title">インターネット企業　新規事業開発責任者　～2000万円程度（非公開）</span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1030261">官民ファンド　中堅企業向け活性化ファンド経営支援担当　～1400万円程度</a></span></li>
                   <li><span class="title">総合商社　ビジネスコンサルティング室　～2000万円程度（非公開）</span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1030220">大手IT企業　プロダクトマネージャー/消費財事業本部　～1300万円程度</a></span></li>
                   <li><span class="title"><a href="http://kc-consul.com/pickjob/#related_list_job_1028284">デジタル系ファーム　事業戦略コンサルタント　～1800万円程度</a></span></li>
                      </ul>

                   	<div class="content_note_item_picup pc clear">
                    	※非公開案件のため、詳細を掲載できないものがありますので、ご興味をお持ちの求人がある場合は、 <a href="<?php echo url_root; ?>entry/?entry_id=1014585">こちら</a>  からご相談ください。
                    </div><!--content_note_item_picup-->
                    </div>
                 </div>
             <?php endif; ?>


         	<div class="title_category_job clear"><span>職種で探す</span></div>
            
            <div class="content_category clear">
            	<ul class="job_list_parent_item clear">
                <?php 
                
                
                                    $query_list=HCMListCategory();
                                    
                                    while($row_list=mysql_fetch_assoc($query_list))
                                    {
                                        $id_category=$row_list['id'];
                                        $query_check=Check_HCMCategory($id_category);
                                        
                                        if($query_check>0 )
                                        {
                                            if($id_category==9 || $id_category==2):
                                               if($row_list['job_count']>=1):	
                                            ?>
         		<li>
                <div class="title_f1 category_jobs_parent_toggle clear">
				<p class="category_jobs_parent">
                <a href="<?php echo url_root; ?>category/job_group/<?php echo $row_list['id'].".html"; ?>" name="<?php echo $row_list['name']; ?>" class="hcm_category_lisst" ><span class="category_txt">
                                            <?php 
                                            echo $row_list['name'];
											?></a>
				<em class="job_count_break">
                  <a class="title_f2 category_jobs_parent_toggle" href="<?php echo url_root; ?>category/list/short/new/<?php echo $row_list['id'].".html"; ?>">(新着&nbsp;<span class="c00"><?php 
					//echo $list_view['new_count'];
					//$cat_id_news=$list_view['id'];
					$view_news_cat="`J`.`new_flag`=1 and ";
					$numrows_news=Count_ListJob_ByCategory($id_category,$view_news_cat);
					echo (int)$numrows_news;
			   ?></span>&nbsp;件</a><a class="title_f1 class="title_f1"" href="<?php echo url_root; ?>category/job_group/<?php echo $row_list['id'].".html"; ?>">/全&nbsp;<span class="c02"><?php 
				//echo $list_view['job_count']."<br/>"; 
				$numrows_news1=Count_ListJob_ByCategory($row_list['id']);
				echo (int)$numrows_news1;
			?></span>&nbsp;件)
            </a>                          
                  </em>                          
                  </p>                     
                                           
                   <p class="toggle_btn mobile fr"></p>
                  </div>
                                        
                                         <!--Get Sub Category----------------------------->
                        <ul class="job_list_child <?php if($id_category==9): echo "job_list_child_fix"; endif; ?> clear">
                            <?php 
                                $query_yes=HCMCategory_Parent_list($id_category);
                            //print_r($query);
                            while($list_view=mysql_fetch_array($query_yes))
                            {
								$cat_id_news=$list_view['id'];
                                if($list_view['job_count']>=1):
                                ?>
                            <li>
                 <a class="title_f1 hcm_category_lisst" href="<?php echo url_root; ?>category/list/<?php echo $list_view['id'].".html"; ?>">
            	<span class="cat_child_1"><?php echo $list_view['name'];  ?></span>
		</a>
        <em class="cat_list_child_block"><a class="title_f2 hcm_category_lisst" href="<?php echo url_root; ?>category/list/short/new/<?php echo $list_view['id'].".html"; ?>">(新着&nbsp;<span class="c00"><?php 
					//echo $list_view['new_count'];
					//$cat_id_news=$list_view['id'];
					$view_news_cat="`J`.`new_flag`=1 and ";
					$numrows_news=Count_ListJob_ByCategory($cat_id_news,$view_news_cat);
					echo (int)$numrows_news;
			   ?></span>&nbsp;件</a><a class="title_f1 class="title_f1"" href="<?php echo url_root; ?>category/list/<?php echo $list_view['id'].".html"; ?>">/全&nbsp;<span class="c02"><?php 
				//echo $list_view['job_count']."<br/>"; 
				$numrows_news1=Count_ListJob_ByCategory($cat_id_news);
				echo (int)$numrows_news1;
			?></span>&nbsp;件)
            </a></em>
                            </li>
                            
                                <?php 
                                else:
                                ?>
                                <li>
                                 <div class="sub_title_name"><?php echo $list_view['name'];  ?>&nbsp;(<span class="c00 black"><?php echo $list_view['job_count']; ?></span>件)</div>
                                </li>
                            <?php
                                 endif;
                            	}
                          	?>
                   </ul>  
                   
               </li>
                            
                            <!--ENd Get Sub Category ------------------------->
                                      								
                                            <?php 
                                                 endif;
                                                endif;
                                                
                                            }
                                            else
                                            {
                                                if($id_category!=1 && $id_category==3 )
                                                    {
                                                        if($row_list['job_count']>=1):
                                        ?>
                                         <li>
                                         
                                         
                                 			<a href="<?php echo url_root; ?>category/list/<?php echo $row_list['id'] .".html";?>" name="<?php echo $row_list['name']; ?>" class="hcm_category_lisst_f1" ><span class="category_txt">
                                            <?php 
                                            echo $row_list['name'];
                                            //echo "&nbsp;({$row_list['job_count']})</span>";
											?>
                                            </a>
                                            <em class="job_count_break">
                                            <a href="<?php echo url_root; ?>category/list/short/new/<?php echo $row_list['id'] .".html";?>" name="<?php echo $row_list['name']; ?>" class="hcm_category_lisst_f2" >
                                            	(新着 <span class="c00"> <?php 
													$view_news_cat_s="`J`.`new_flag`=1 and ";
													$numrows_news_s=Count_ListJob_ByCategory($id_category,$view_news_cat_s);
													echo (int)$numrows_news_s;
												?></span> 件
                                                
                                            </a>
                                             <a href="<?php echo url_root; ?>category/list/<?php echo $row_list['id'] .".html";?>" name="<?php echo $row_list['name']; ?>" class="hcm_category_lisst_f3" >
                                            	/全 <span class="c02"><?php echo "&nbsp;".Count_ListJob_ByCategory($row_list['id'])." "; ?></span>件)
                                            </a>
                                            </em>
                                            </li>
                                       
                                        <?php 
                                                else:
                                            ?>
                                  		 <ul class="job_list_child clear">
                               				<li>
                                            <?php 
                                           		 echo $row_list['name'];
                                           		// echo "&nbsp;({$row_list['job_count']})";
											 	echo "&nbsp;".Count_ListJob_ByCategory($row_list['id']);
                                             ?>
                                            </li>
                                      	 </ul>
                                       </li>
                                            <?php
                                                    endif;
                                                    }
                                            }
                                    }
                                        
                                    ?>
                                    
                                    
                                 <li>
                                           <a class="hcm_category_lisst" name="" href="<?php echo url_root; ?>fund/pe/page/1">ファンド（PE・VC) 
                                           <em class="job_count_break">
											<?php 
				$where_pe="";
				$where_pe .=" and `J`.`industry_code` like  '%PEファンド%' ";
	 	$Count_Job_pe=Get_Data("`job_category` as `C` join `job` as `J` on `C`.`job_id`=`J`.`id` join category as `D` on `C`.`category_id`=`D`.`id`",$where_pe, "count(DISTINCT `J`.`order_id`) as cntt");
				$numrows_all = $Count_Job_pe['cntt'];	
						$Count_Job_pe_new=Get_Data("`job_category` as `C` join `job` as `J` on `C`.`job_id`=`J`.`id` join category as `D` on `C`.`category_id`=`D`.`id`",$where_pe." and `J`.`new_flag`=1", "count(DISTINCT `J`.`order_id`) as cntt");	
						$numrows_new = $Count_Job_pe_new['cntt'];				
				echo " (新着 <span class='c00'>".$numrows_new."</span> 件  /全 <span class='c02'> ".$numrows_all."</span> 件)";
											?>   
                                                 </em>                            
                                           </a>
                                  </li>
                                    
                                    
                      </ul>              
            </div>
            
            
            <div class="search_job clear">
             <form class="search_form_kc search_form_query" method="post" action="<?php echo url_root; ?>search/0/page/1/<?php echo $_POST['frm_txt']; ?>">
            	<!--<div class="search_category l">
                	 <h3 class="search">年収で探す</h3>
                      <select class="select_form1 category_select_search salary_option_job" name="search_select_salary" id="search_select_category">
                              <option value="">ご希望の年収を選択してください。</option>
                              <option  value="<?php echo url_root; ?>find/salary/page/1/-800万円">～800万円</option>
                              <option value="<?php echo url_root; ?>find/salary/page/1/800万円-1000万円">800万～1000万円</option>
                              <option value="<?php echo url_root; ?>find/salary/page/1/1000万円-1500万円">1000万～1500万円</option>
                              <option value="<?php echo url_root; ?>find/salary/page/1/1500万円-">1500万円</option>      
                      </select>
                </div>-->
                <div class="search_post_txt r" style="float:none; width:100%;">
                    	<h3 class="search">キーワードで探す</h3>
                   		<input type="text" name="frm_txt" class="search_form_txt_index text_search search_form_txt"  style=" width:545px;"/>
                        <span class="button_search"><input type="submit" value="検索" class="button_submit" /></span>
                   
                </div>
             </form>    
            </div>
            
        </div><!--job_info_list-->
        <!--End Job info-->
        
        
        <div class="clear group_note_sd pc">
        	<div class="clear content_group_note_sd">
	        	<h3><img src="<?php echo url_root; ?>img/jobinfo/secret-job-title.png" border="0" alt="非公開求人を紹介してもらう"/></h3>
	            <p>
	            当社だけに特別にオーダーをいただいている公開できないコンフィデンシャル案件もございます。非公開求人も含めてご紹介をご希望の方はこちらから<a href="<?php if(isset($_GET['entry_id'])){
						$link_entry_easy.='?entry_id='.$_GET['entry_id'];
					}else{
						$link_entry_easy.='?entry_id=1014585';
					}
					echo url_root."entry/".$link_entry_easy;?>" target="_blank">お申込み</a>ください。
	            </p>
                <div class="secret_entry_btn"><a href="<?php echo url_root."entry/?entry_id=1014585"; ?>" target="_blank"><img src="<?php echo url_root; ?>img/entry/button-secret-job-entry.png" alt="非公開求人を紹介してもらう"/></a></div>
            </div>
        </div><!--group_note_sd-->
        
        
        
        
        
        
        
        
        
        
        
        
        