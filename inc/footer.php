 <footer id="colophon" class="footer_inc clear" role="contentinfo">
 		
        <div class="clear footer_break_top">
        	<div class="clear wauto">
        		<div class="l footer_break_top_left">
                	<div class="clear group_break_top_left">
                    	<div class="title_break_top_left l">このサイトをシェアする</div>
                        <div class="social_break_top_left l">
                        	<ul class="clear social_list_item">
                        		<li class="fb_foot_btn">
            <div class="fb-like" data-href="<?php echo url_root; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                                </li>
                        		<li class="twitt_foot_btn">
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo url_root; ?>" data-lang="ja" target="_blank">ツイート</a>
                                </li>
                        		<li class="gplus_foot_btn">
             <div class="g-plusone" data-size="medium" data-href="<?php echo url_root; ?>"></div>
                                </li>
                        	</ul>
                        </div><!--social_break_top_left-->
                    </div>
                </div><!--footer_break_top_left-->
                
            	<div class="r footer_break_top_right">
                	<a id="back-top" href="javascript:void(0);">
                    	<span class="pc_show">BACK TO TOP ▲ </span>
                        <span class="mobile_show"><img src="<?php echo url_root; ?>img/button-page-top.png" alt="button page"/></span>
                    </a>
                </div><!--footer_break_top_right-->
                
            </div>
        </div><!--footer_break_top-->
 		
        
        <div class="footer_content clear">
        	<div class="clear footer_content_item_group">
	        	<div class="footer_content_one">
                	<div class="item_content_footer clear">
	            		<h3 class="title_ft"><img src="<?php echo url_root; ?>img/home/footer-main-title.png" alt="Consultant Career" /></h3>
                        <div class="clear">
	                        <ul class="footer_item_link_1 ">
	                        	<li><a href="<?php echo url_root;?>">ホーム</a></li>
                                  	<li><a href="<?php echo url_root; ?>interview/">インタビュー</a>
                                	<ul>
                                    	 <li><a href="<?php echo url_root; ?>interview/#inexperience">未経験からコンサルタント</a></li>
                                         <li><a href="<?php echo url_root; ?>interview/#consultant-post">コンサルタントから次のキャリアへ</a></li>
                                         <li><a href="<?php echo url_root; ?>interview/#top">トップインタビュー</a></li>
                                         <li><a href="<?php echo url_root; ?>interview/#symposium">コンサルタント座談会</a></li>
                                    </ul>
                                </li> 
                                 <li><a href="<?php echo url_root; ?>job-search/">求人検索</a>
                                	<ul>
                                    	<li><a href="<?php echo url_root; ?>category/job_group/2.html" >ビジネスコンサルタント</a></li>
                                        <li><a href="<?php echo url_root; ?>category/list/3.html" >経営企画・事業企画 </a></li>
                                        <li><a href="<?php echo url_root; ?>category/job_group/9.html" >ITコンサルタント</a></li>
                                        <li><a href="<?php echo url_root; ?>fund/pe/page/1" >ファンド（PE・VC)</a></li>
                                    </ul>
                                </li>          
                                
	                                
	                        </ul>
	                         <ul class="footer_item_link_2">
                             <li><a href="<?php echo url_root; ?>about-us/#policy_part">サービス理念</a></li>
                              <li><a href="<?php echo url_root; ?>about-us/#consultant_part">コンサルタント紹介</a></li>
	                        <li><a href="<?php echo url_root; ?>about-us/#profile_part">会社概要</a></li>
                            <li><a href="<?php echo url_root; ?>blog/">コンサルタント転職のこぼれ話</a></li>
                            <li><a href="<?php echo url_root; ?>entry/?entry_id=1014585" target="_blank">キャリアサポートを申し込む</a></li>
                            <li><a href="<?php echo url_root; ?>sitemap/">サイトマップ</a></li>
	                            <!--<li><a href="<?php echo url_root; ?>seminar/">セミナー情報</a></li> -->
	                         <li><a href="<?php echo url_root_main; ?>kc-recruit/entry/consultant/" target="_blank" rel="nofollow">自社採用<span class="icon_add"></span></a></li>     
                               
	                        </ul>
                        </div>
                    </div><!--item_content_footer-->
	            </div><!--footer_content_one-->
	            
	            <div class="footer_content_two">
                	<div class="item_content_footer clear">
	            		<h3 class="title_ft">プレミアム企画<span class="icon_add"></span></h3>
                        <ul class="clear link_content_kandc">
                        	<li>
                            <a rel="nofollow" target="_blank" href="<?php echo url_root_main; ?>exe/" >
                            	<img src="<?php echo url_root; ?>img/link/link-exe.png" alt="エグゼクティブポジションでの転身・転職のご支援　EXE"/>
                            </a>
                            </li>
                        	<li>
                            <a rel="nofollow" target="_blank" href="<?php echo url_root_main; ?>amazontour/" >
                            	<img src="<?php echo url_root; ?>img/link/link-amazon.png" alt="当社からご紹介のamazon現役社員6人にインタビュー"/>
                            </a>
                            </li>
                        	<li>
                            <a rel="nofollow" target="_blank" href="<?php echo url_root_main; ?>turning-point/" >
                            	<img src="<?php echo url_root; ?>img/link/link-turing-point.png" alt="転機をチャンスに変えた瞬間 Turning Point"/>
                            </a>
                            </li>
                        	<li>
                            <a rel="nofollow" target="_blank" href="<?php echo url_root_main; ?>career_up/consultant/19.html" >
                            	<img src="<?php echo url_root; ?>img/link/link-careerup-coulmn.png" alt="キャリアアップコラム　転職・キャリア構築にまつわる当社コンサルタントのコラム"/>
                            </a>
                            </li>
                        	<li>
                            <a rel="nofollow" target="_blank" href="<?php echo url_root_main; ?>career/#career_cate_1401694028" >
                            	<img src="<?php echo url_root; ?>img/link/link-career.png" alt="個別キャリア相談会開催中 キャリア構築、転職活動の第一歩としてお気軽にご参加ください。"/>
                            </a>
                            </li>
                        </ul>
                    </div><!--item_content_footer-->
	            </div><!--footer_content_two-->
	            
	            <div class="footer_content_three">
                	<div class="item_content_footer clear">
	            		<h3 class="title_ft">社外メディアへの執筆<span class="icon_add"></span></h3>
                        <ul class="clear link_content_diamond">
                        	<li>
                            <a rel="nofollow" target="_blank" href="http://diamond.jp/category/s-mtenshoku/" >
                            	<img src="<?php echo url_root; ?>img/link/link-diamond-01.png" alt="転職で幸せになる人、不幸になる人"/>
                            </a>
                            </li>
                        	<li>
                            <a rel="nofollow" target="_blank" href="http://diamond.jp/category/s-35tensyoku2/" >
                            	<img src="<?php echo url_root; ?>img/link/link-diamond-02.png" alt="35歳から転職力養成講座"/>
                            </a>
                            </li>
                        	
                        	<li>
                            <a rel="nofollow" target="_blank" href="http://www.atmarkit.co.jp/ait/kw/head_hunter_maruyama.html" >
                            	<img src="<?php echo url_root; ?>img/link/link-it.png" alt="ヘッドハンター丸山の「いつか、あなたに逢いにいく」"/>
                            </a>
                            </li>
                            <li>
                            <a rel="nofollow" target="_blank" href="http://diamond.jp/articles/-/77473?page=3" >
                            	<img src="<?php echo url_root; ?>img/link/link-diamond-03.png" alt="部下に「同行してください」と言われない上司はヤバイ！10年後も生き残る管理職の条件【前編】"/>
                            </a>
                            </li>
                             <li>
                            <a rel="nofollow" target="_blank" href="http://diamond.jp/articles/-/77642" >
                            	<img src="<?php echo url_root; ?>img/link/link-diamond-04.png" alt="機械に仕事を奪われないために今からやるべきこと"/>
                            </a>
                            </li>
                        </ul>
                    </div><!--item_content_footer-->
	            </div><!--footer_content_three-->
            </div>
        </div><!--footer_content-->
        
       <div class="copyright_footer clear">
       		<div class="footer_content clear">
	       		<div class="l">
	            	<a href="<?php echo url_root_main; ?>?referer=kc-footer" target="_blank"><img src="<?php echo url_root; ?>img/home/logo-kreis.png" alt="クライス&amp;カンパニー" /></a>
	            </div>	
	       		<div class="r">
	              Copyright &copy; <?php echo date('Y'); ?> KREIS&Company Inc. All Right Reserved. webdesign <a href="http://tokyodesignroom.com" rel="nofollow">tokyodesignroom.com</a>
	            </div>
            </div><!--footer_content-->
       </div><!--copyright_footer-->
 
    	
    </footer>
</div>    
<script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41366618-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- User Insight PCDF Code Start : kc-consul.com -->
<script type="text/javascript">
<!--
var _uic = _uic ||{}; var _uih = _uih ||{};_uih['id'] = 51748;
_uih['lg_id'] = '';
_uih['fb_id'] = '';
_uih['tw_id'] = '';
_uih['uigr_1'] = ''; _uih['uigr_2'] = ''; _uih['uigr_3'] = ''; _uih['uigr_4'] = ''; _uih['uigr_5'] = '';
_uih['uigr_6'] = ''; _uih['uigr_7'] = ''; _uih['uigr_8'] = ''; _uih['uigr_9'] = ''; _uih['uigr_10'] = '';

/* DO NOT ALTER BELOW THIS LINE */
/* WITH FIRST PARTY COOKIE */
(function() {
var bi = document.createElement('scri'+'pt');bi.type = 'text/javascript'; bi.async = true;
bi.src = ('https:' == document.location.protocol ? 'https://bs' : 'http://c') + '.nakanohito.jp/b3/bi.js';
var s = document.getElementsByTagName('scri'+'pt')[0];s.parentNode.insertBefore(bi, s);
})();
//-->
</script>
<!-- User Insight PCDF Code End : kc-consul.com -->

<!-- リマーケティング タグの Google コード -->
<!--------------------------------------------------
リマーケティング タグは、個人を特定できる情報と関連付けることも、デリケートなカテゴリに属するページに設置することも許可されません。タグの設定方法については、こちらのページをご覧ください。
http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 967277328;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/967277328/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript> 
    
<!--G+ script-->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>


<!--Twitter script-->
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));</script>


<!--facebook script-->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId      : '1028623883855019',
      xfbml      : true,
      version    : 'v2.5'
    });
  };
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'Q1Z3P4H4UG';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="https://b92.yahoo.co.jp/js/s_retargeting.js"></script>

<!-- Yahoo Code for your Target List -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_ss_retargeting_id = 1000377546;
var yahoo_sstag_custom_params = window.yahoo_sstag_params;
var yahoo_ss_retargeting = true;
/* ]]> */
</script>
<script type="text/javascript" src="https://s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://b97.yahoo.co.jp/pagead/conversion/1000377546/?guid=ON&script=0&disvt=false"/>
</div>
</noscript>


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869777849;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/869777849/?guid=ON&amp;script=0"/>
</div>
</noscript>
