<?php 
	ob_start();
?>
<?php
@include('../Lib/_init.php');
@include('../config.php');

if ($protocol == 'http:' || $protocol =='HTTP:'){
   // $entry_protocol = str_replace('http', 'https', curPageURL() );
   // header('Location:'.$entry_protocol);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 6]>
<html id="ie6" class="ie" dir="ltr" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" class="ie" dir="ltr" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" class="ie" dir="ltr" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html id="noIE" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="keywords" content="ヘッドハンティング,人材紹介会社,転職エージェント,キャリアコンサルティング,クライス&カンパニー,30代,コンサルタント,エンジニア,キャリアデザイン,転職サポート,エントリー"/>
    <meta name="description" content="クライス&カンパニーのサービスへのエントリーフォームです。エントリーご希望の方は、下記項目にご入力の上、ご送信ください。私たちが、あなたの良きパートナーとして転職活動をサポートいたします。"/>
    <meta name="author" content="株式会社クライス・アンド・カンパニー" />
    <meta name="copyright" content="Copyright&copy;2007 KREIS&amp;Company Inc.　All Right Reserved." />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Language" content="ja" />
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
    <title>キャリアサポートを申し込む | ヘッドハンティング・人材紹介会社・転職のクライス&amp;カンパニー</title>
    <!--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="<?php echo url_root_main; ?>js/jquery.min.js"></script>
    <script src="<?php echo url_root_main; ?>js/show_form.js" type="text/javascript"></script>
    
    <script src="<?php echo url_root_main; ?>js/watermark.js" type="text/javascript"></script>
    <script src="<?php echo url_root; ?>js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script  src="<?php echo url_root; ?>js/jquery.flexnav.js"></script>
      <script  src="<?php echo url_root; ?>js/kc-consul_main.js"></script>
      <script  src="<?php echo url_root; ?>js/mobile_main.jquery.js"></script>
      <script  src="<?php echo url_root; ?>js/mobile.js"></script>
    <!-- css -->
     <link type="text/css" href="<?php echo url_root_main; ?>css/style.css" rel="stylesheet"/>
   <link type="text/css" href="<?php echo url_root; ?>css/style.css" rel="stylesheet"/>
   <link type="text/css" href="<?php echo url_root; ?>css/flexnav.css" rel="stylesheet"/>
   <link type="text/css" href="<?php echo url_root; ?>css/mobile_style.css" rel="stylesheet"/>
   <link type="text/css" href="style.css" rel="stylesheet"/>

    <!-- javascript -->
    <!--<script type="text/javascript" src="js/scroller.js"></script>-->
    <script type="text/javascript">
        $(document).ready(function () {
            // hide #back-top first
            $("#back-top").hide();
            // fade in #back-top
            $(function () {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 100) {
                        $('#back-top').fadeIn();
                    } else {
                        $('#back-top').fadeOut();
                    }
                });
                // scroll body to 0px on click
                $('#back-top a').click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });
            });
        });
    </script>
    
<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=179447,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
    
    
</head>

<body>
<?php 
	if(isset($_POST['m1id'])):
		$m1id=(int)$_POST['m1id'];
		if($m1id>0):
	?>
<?php 
		$date_curent=date('Y年m月d日 H:i');
	?>

<!-- EBiS tag version2.11 start --> 
<script type="text/javascript">
<!--
if ( location.protocol == 'http:' ){
    strServerName = 'http://ac.ebis.ne.jp';   
} else {
    strServerName = 'https://ac.ebis.ne.jp';
}
cid = '4T5ZVtUs'; pid = 'consul_entry'; m1id='<?php echo $m1id; ?>'; a1id=''; o1id='<?php echo $_POST['o1id']; ?>'; o2id='<?php echo $_POST['o2id']; ?>'; o3id='<?php echo $_POST['o3id']; ?>'; o4id='<?php echo $_POST['o4id']; ?>'; o5id='<?php echo $date_curent; ?>';document.write("<scr" + "ipt type=\"text\/javascript\" src=\"" + strServerName + "\/ebis_tag.php?cid=" + cid + "&pid=" + pid + "&m1id=" + m1id +"&a1id=" + a1id + "&o1id=" + o1id + "&o2id=" + o2id + "&o3id=" + o3id + "&o4id=" + o4id + "&o5id=" + o5id + "\"><\/scr" + "ipt>");
// -->
</script>
<noscript>
<img src="//ac.ebis.ne.jp/log.php?argument=4T5ZVtUs&ebisPageID=consul_entry&ebisMember=&ebisAmount=&ebisOther1=&ebisOther2=&ebisOther3=&ebisOther4=&ebisOther5=" width="0" height="0">
</noscript>
<!-- EBiS tag end -->

<?php 
		endif;
	endif;
?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NFKWDH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NFKWDH');</script>
<!-- End Google Tag Manager -->

<div class="clear page_main entry_forms_page">
<header id="header_inc" class="clear">

   <div class="header_top clear">
         	<div class="header_top_content clear">
        	<div class="l site_title">
	            	<ul class="clear top_break">
	                     <li><a href="<?php  echo url_root_main; ?>" target="_blank">30代・40代転職</a></li>
	                    <li><a href="<?php  echo url_root_main; ?>exe/" target="_blank">エグゼクティブ転職</a></li>
						<li><a href="<?php  echo url_root; ?>" class="active">コンサルタント転職</a></li>
					</ul>
	           </div><!--site_title-->
               
	           <div class="social_btns r clear social_btns_header">
               		<div class="l title_product_by_hd">produced by</div>
               		<div class="l kandc_title_product_by_hd"><a href="<?php  echo url_root_main; ?>" target="_blank"><img src="<?php echo url_root; ?>img/link/head-kreis-name.png" alt="30代・40代転職"/></a></div>
               		
               		<!--<div class="gplus_btn r" style="width:57px;"><g:plusone size="medium" href="<?php echo $pageURL_cu; ?>"></g:plusone></div>
					<div class="twitter_btn r">
	            		<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $pageURL_cu; ?>" rel="nofollow">Tweet</a>
	            	</div>  
		            <div class="fb_btn r">
	                	<div class="fb-like" data-href="<?php echo $pageURL_cu; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
	                </div>-->
                </div><!--end .social_btns-->
           </div> <!--End .header_top_content-->
         </div><!--End .header_top-->
         
         
        <div class="header_logo clear">
        	<div class="header_top_content header_logo_content clear">
            	<div class="l">
            	<h2 class="clear">
                <a href="<?php echo url_root; ?>">
                	<img src="<?php echo url_root; ?>img/home/logo-consul.png" alt="Consultant Career"/></a>
                    
                </h2>
                <h1 class="clear">コンサルタントの転職・キャリアを共に考える</h1>
                </div>
                <?php /*?><div class="button_entry r">
                <!--<a class="go_entry_consultant" href="http://www.kandc.com/kc-recruit/entry/consultant/" target="_blank"><img src="<?php echo url_root; ?>img/link/button_recruit.png" alt="自社採用 コンサルタント職 募集中"/></a>-->
                <a class="entry_tbl_kc pc" href="<?php

				
			echo url_root."entry_easy/?entry_id=1014585";

?>" target="_blank"><img src="<?php echo url_root; ?>img/entry/entry-easy-button.png" alt="まずは気軽に相談する" /></a>
<a class="entry_tbl_kc mobile" href="<?php

				echo url_root."entry_easy/?entry_id=1014585";

?>" target="_blank"><img src="<?php echo url_root; ?>img/entry/entry-easy-footer.png" alt="まずは気軽に転職相談する" /></a>
</div><?php */?>
            </div>
        </div><!--End .header_logo-->
		
 </header>  
<header id="header_area" class="container hide_on_pc mobile">
	<div id="top_area" class=" clear">
    	<div class="l">
            	<h1 class="big_title clear">
                    <a href="<?php echo url_root; ?>">
                        <img src="<?php echo url_root; ?>img/home/logo-consul.png" alt="Consultant Career">
                    </a>                    
                </h1>
                <h2 class="subtitle clear"><a href="<?php echo url_root; ?>">コンサルタントの転職・キャリアを共に考える</a></h2>
        </div>
        
        <div class="r menu_btn_area">
        	<a href="javascript:void(0);" class="mobile_menu_btn open_icon r">Toggle Menu</a>
   			<ul id="mobile_primary_menu" class="clear">
                    <li>
                      <a class="home" href="<?php echo url_root; ?>">
                      <span>ホーム</span>
                      </a>
                    </li>
                     <li>
                        <a class="policy" href="<?php echo url_root; ?>about-us/">
                        <span>クライスの特徴</span></a>
                    </li>
                    <li>
                        <a class="interview" href="<?php echo url_root; ?>interview/"><span>インタビュー </span></a>
                    </li>
                    
                    <li>
                        <a class="job-search" href="<?php echo url_root; ?>job-search/"><span>求人情報</span></a>
                    </li>
                 
                    <li>
                        <a class="blog" href="<?php echo url_root; ?>blog/"><span>プログ</span></a>
                    </li>
               
            </ul>
       </div> <!--menu_btn_area-->
    </div>
   
</header> 
 
<div id="entry" class="clear">
        <div id="entry-nav">
            <ul class="clear">
                <li><span class="entry-nav-step"></span></li>
                <li><span class="entry-nav-step1-none"></span></li>
                <li><span class="entry-nav-step2"></span></li>
                <li><span class="entry-nav-step3-active"></span></li>
            </ul>
        </div>
    </div>

<div id="container">
<div id="content_consul">

<div id="entry-form" class="entry-form entry-form-complete">
<form action="" method="">

<!-------------------------------------------------------------->
<div class="form-personal-profile" style="width:700px; margin:20px auto;">
    <div class="title-top">お申し込みありがとうございます。</div>
    <div class="thank-for-you">
        <p>担当コンサルタントまたは候補者サポート事務局より、<br />ご登録いただいたお電話番号またはメールアドレス宛にご連絡いたします。<br /><br />
			  尚、1週間以内に弊社からのご連絡がない場合は、<br />ご連絡先の誤入力の可能性がございますので、<br />お手数おかけいたしますが下記までご一報くださいませ。<br /><br />
			  また、ご経験やご希望の業界、職種、ご年齢などによっては<br />ご紹介が難しい場合がございます。予めご了承ください。<p>
    </div>
    <div class="contact-personal">
        <div class="contact-personal-title">お問合せ</div>
        <div>TEL  03-5733-1602</div>
        <div>Email  <a href="mailto:kreis-info@kandc.com">kreis-info@kandc.com</a></div>
    </div>
</div>
<!--------------------------------------->
<div class="clear">
    <a class="button-complete-page" href="<?php echo url_root; ?>"><img src="../img/entry/back-home-button.png" alt="ヘッドハンティングのクライス＆カンパニー"></a>

    <!--    <div class="form-handler"><a href="#" title="Submit next Step 2"><img src="../img/entry/button-confirm.png" alt=""></a>-->
    <!--    </div>-->
</div>
</form>
</div>

</div>
<!--<div style="margin: 0 auto; width: 980px" class="clear">
    <div id="back-top">
        <a href="#top"><img src="../img/features/back-to-up.png"></a>
    </div>
</div>-->

</div>

<footer id="colophon" class="footer_inc clear" role="contentinfo">
    <div class="clear footer_entry_frm footer_content ">
    	<div class="l">
	              Copyright &copy; <?php echo date('Y'); ?> KREIS&amp;Company Inc. All Right Reserved. webdesign <a rel="nofollow" href="http://tokyodesignroom.com">tokyodesignroom.com</a>
		</div>
       <!-- <div id="footer" class="clear">
            <div class="copy">
                Copyright &copy;<?php echo date('Y'); ?> KREIS &amp; Company Inc. All Right Reserved. webdesign tokyodesignroom.com
            </div>
        </div>-->
     </div>
</footer>
</div>



<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000240003;
var yahoo_conversion_label = "1VzZCOfltF8QgvPtvgM";
var yahoo_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="https://s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://b91.yahoo.co.jp/pagead/conversion/1000240003/?value=0&amp;label=1VzZCOfltF8QgvPtvgM&amp;guid=ON&amp;script=0&amp;disvt=true"/>
</div>
</noscript>



<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_ydn_conv_io = "_LaOawsOLDXsoLVLLP1r";
var yahoo_ydn_conv_label = "6J0YQD0HU03ML6NUWTO121495";
var yahoo_ydn_conv_transaction_id = "";
var yahoo_ydn_conv_amount = "0";
/* ]]> */
</script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="//b90.yahoo.co.jp/conv.js"></script>




<script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_ydn_conv_io = "but68SgOLDVw89tkLGbn";
  var yahoo_ydn_conv_label = "LKHKDQAWZGUA3J1W44L375487";
  var yahoo_ydn_conv_transaction_id = "";
  var yahoo_ydn_conv_amount = "0";
  /* ]]> */
</script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="https://b90.yahoo.co.jp/conv.js"></script>
<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var yahoo_conversion_id = 1000377546;
    var yahoo_conversion_label = "xO78CNC2pnQQ26TTmQM";
    var yahoo_conversion_value = 0;
    /* ]]> */
</script>
<script type="text/javascript" src="https://s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="https://b91.yahoo.co.jp/pagead/conversion/1000377546/?value=0&label=xO78CNC2pnQQ26TTmQM&guid=ON&script=0&disvt=true"/>
    </div>
</noscript>

<!-- Google Code for &#12467;&#12531;&#12469;&#12523;_&#12461;&#12515;&#12522;&#12450;&#12469;&#12509;&#12540;&#12488;&#30003;&#12375;&#36796;&#12415; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869777849;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "jtUWCK6vpnQQuYPfngM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/869777849/?label=jtUWCK6vpnQQuYPfngM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>




</body>
</html>
